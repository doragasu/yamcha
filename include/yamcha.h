#ifndef __STRIP_ANIM_H__
#define __STRIP_ANIM_H__

#include <stdint.h>

#define YAM_PIXEL_NUM 8

struct yam_pixel {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

enum yam_direction {
	YAM_DIRECTION_INC,
	YAM_DIRECTION_DEC,
	__YAM_DIRECTION_MAX
};

#define YAM_CMD_TABLE(X_MACRO) \
	X_MACRO(OFF, off) \
	X_MACRO(FIXED, fixed) \
	X_MACRO(BREATH, breath) \
	X_MACRO(SLIDE, slide) \
	X_MACRO(PINGPONG, pingpong) \

#define X_AS_CMD_ENUM(uname, lname) YAM_CMD_ ## uname,
enum yam_mode {
	YAM_CMD_TABLE(X_AS_CMD_ENUM)
	__YAM_CMD_MAX
};

struct yam_cfg_off {
	// Nothing to configure
};

struct yam_cfg_fixed {
	struct yam_pixel pixels[YAM_PIXEL_NUM];
};

struct yam_cfg_breath {
	struct yam_pixel pixels[YAM_PIXEL_NUM];
	uint32_t delay_ms;
	enum yam_direction initial_dir;
	const struct yam_cmd_req *link_top;
	const struct yam_cmd_req *link_bot;
};

struct yam_cfg_slide {
	struct yam_pixel pixels[YAM_PIXEL_NUM];
	enum yam_direction dir;
	uint32_t delay_ms;
	struct yam_cmd_req *link;
};

struct yam_cfg_pingpong {
	struct yam_pixel initial_value;
	enum yam_direction initial_dir;
	uint32_t steps;
	uint32_t delay_ms;
	const struct yam_cmd_req *link_top;
	const struct yam_cmd_req *link_bot;
};

#define X_AS_CFG_UNION(uname, lname) struct yam_cfg_ ## lname lname;
struct yam_cmd_req {
	enum yam_mode mode;
	union {
		YAM_CMD_TABLE(X_AS_CFG_UNION)
	};
};

// Initialize the module. Requires the GPIO number used for the LED strip
int yam_init(uint_fast8_t gpio);

// Deinitialize the module and free resources.
void yam_deinit(void);

// Send a command to the module.
// WARNING: the lifetime of the input request structure must be equal or
// greater than the time the effect needs to be displayed.
void yam_command(const struct yam_cmd_req *req);

#endif /*__STRIP_ANIM_H__*/

