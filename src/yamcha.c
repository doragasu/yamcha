#include <string.h>

#include <FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <driver/hw_timer.h>
#include <esp8266/gpio_struct.h>

#include <yamcha.h>

// TODO: move ws2812 write routines to a driver module

#define TMR_CMD_TICKS pdMS_TO_TICKS(5000)

#define TMR_RELOAD_VAL(ms) ((ms) * (80e3 / 256))

// Scales a value using other 8-bit scale number.
#define SCALE_8BIT(value, scale) (((value) * (scale))>>8)

// Number of points of the sin2 LUT
#define SIN2_POINTS 32

// sin^2(x) from 0 to pi/2
static const uint8_t sin2[SIN2_POINTS] = {
	0x00, 0x01, 0x03, 0x06, 0x0A, 0x10, 0x17, 0x1F,
	0x28, 0x31, 0x3C, 0x47, 0x53, 0x60, 0x6C, 0x79,
	0x86, 0x93, 0x9F, 0xAC, 0xB8, 0xC3, 0xCE, 0xD7,
	0xE0, 0xE8, 0xEF, 0xF5, 0xF9, 0xFC, 0xFE, 0xFF
};

// Use a relatively high priority to avoid disturbing the animation timings
#define ANIM_TSK_PRIO (tskIDLE_PRIORITY + 5)
#define YAM_DELAY_NS_400
#define YAM_DELAY_NS_800 for (volatile uint32_t i = 0; i > 0; i--);

enum yam_event {
	YAM_EVENT_UPDATE = 1,
	YAM_EVENT_TIMER = 2
};

struct yam_stat_off {
	// No stat required
};

struct yam_stat_fixed {
	// No stat required
};

struct yam_stat_breath {
	struct yam_pixel pixels[YAM_PIXEL_NUM];
	enum yam_direction dir;
	uint32_t step;
};

struct yam_stat_slide {
	uint_fast8_t step;
};

struct yam_stat_pingpong {
	struct yam_pixel pixels[YAM_PIXEL_NUM];
	enum yam_direction dir;
	uint_fast8_t step;
	uint_fast8_t pos;
};

#define X_AS_STAT_UNION(uname, lname) struct yam_stat_ ## lname lname;
static struct {
	uint32_t gpio_mask;
	const struct yam_cmd_req *req;
	const struct yam_cmd_req *pend;
	xTaskHandle tsk;
	union {
		YAM_CMD_TABLE(X_AS_STAT_UNION)
	};
} yam;

#define X_AS_INIT_FN_PROTO(uname, lname) static void lname ## _init(void);
YAM_CMD_TABLE(X_AS_INIT_FN_PROTO);

#define X_AS_INIT_FN_TABLE(uname, lname) lname ## _init,
static void (* const init_fn[__YAM_CMD_MAX])(void) = {
	YAM_CMD_TABLE(X_AS_INIT_FN_TABLE)
};

#define X_AS_UPDATE_FN_PROTO(uname, lname) static void lname ## _update(void);
YAM_CMD_TABLE(X_AS_UPDATE_FN_PROTO);

#define X_AS_UPDATE_FN_TABLE(uname, lname) lname ## _update,
static void (* const update_fn[__YAM_CMD_MAX])(void) = {
	YAM_CMD_TABLE(X_AS_UPDATE_FN_TABLE)
};

static void IRAM_ATTR set(void)
{
	GPIO.out_w1ts |= yam.gpio_mask;
}

static void IRAM_ATTR clr(void)
{
	GPIO.out_w1tc |= yam.gpio_mask;
}

static void IRAM_ATTR zero(void)
{
	set();
	YAM_DELAY_NS_400;
	clr();
	// Theoretically I should add a YAM_DELAY_NS_800 here, but it seems
	// it is not required (my theory is that only high level pulse width is
	// measured by the ws2812 and the low level with can be much lower as
	// long as the LED can notice it
}

static void IRAM_ATTR one(void)
{
	set();
	YAM_DELAY_NS_800;
	clr();
	// As happened above, theoretically I should add a YAM_DELAY_NS_400
	// here, but it seems it works great without it
}

static void IRAM_ATTR byte_write(uint_fast8_t byte)
{
	for (uint_fast8_t i = 0; i < 8; i++, byte <<= 1) {
		if (byte & 0x80) {
			one();
		} else {
			zero();
		}
	}
}

static void IRAM_ATTR pixel_write(const struct yam_pixel *pixel)
{
	// ws2812 color order is GRB
	byte_write(pixel->g);
	byte_write(pixel->r);
	byte_write(pixel->b);
}

static void IRAM_ATTR yam_write(const struct yam_pixel *pixels, int num_pixels)
{
	for (size_t i = 0; i < num_pixels; i++) {
		pixel_write(&pixels[i]);
	}
}

static void yam_update(const struct yam_pixel *pixels)
{
	taskENTER_CRITICAL();
	yam_write(pixels, YAM_PIXEL_NUM);
	taskEXIT_CRITICAL();
	// NOTE: 50 us delay for end condition is not needed since we are
	// not updating the pixels that often
}

static void yam_off(void)
{
	taskENTER_CRITICAL();
	for (int i = 0; i < (YAM_PIXEL_NUM * 8 * 3); i++) {
		zero();
	}
	taskEXIT_CRITICAL();
	// Here should go the 50 us delay, but who needs it?
}

static void timer_start(uint32_t tout_ms)
{
	hw_timer_set_load_data(TMR_RELOAD_VAL(tout_ms));
	hw_timer_enable(true);
}

static void off_init(void)
{
	off_update();
}

static void off_update(void)
{
	yam_off();
}

static void fixed_init(void)
{
	fixed_update();
}

static void fixed_update(void)
{
	yam_update(yam.req->fixed.pixels);
}

static void breath_init(void)
{
	memset(&yam.breath, 0, sizeof(struct yam_stat_breath));
	if (YAM_DIRECTION_DEC == yam.req->breath.initial_dir) {
		yam.breath.dir = YAM_DIRECTION_DEC;
		yam.breath.step = SIN2_POINTS - 1;
	}
	breath_update();
}

static void breath_update(void)
{
	const uint32_t scale = sin2[yam.breath.step];
	const struct yam_cfg_breath *breath = &yam.req->breath;

	for (int i = 0; i < YAM_PIXEL_NUM; i++) {
		yam.breath.pixels[i].r = SCALE_8BIT(breath->pixels[i].r, scale);
		yam.breath.pixels[i].g = SCALE_8BIT(breath->pixels[i].g, scale);
		yam.breath.pixels[i].b = SCALE_8BIT(breath->pixels[i].b, scale);
	}

	yam_update(yam.breath.pixels);

	if (YAM_DIRECTION_INC == yam.breath.dir) {
		yam.breath.step++;
		if ((SIN2_POINTS - 1) == yam.breath.step) {
			if (breath->link_top) {
				yam_command(breath->link_top);
				return;
			} else {
				yam.breath.dir = YAM_DIRECTION_DEC;
			}
		}
	} else {
		yam.breath.step--;
		if (0 == yam.breath.step) {
			if (breath->link_bot) {
				yam_command(breath->link_bot);
				return;
			} else {
				yam.breath.dir = YAM_DIRECTION_INC;
			}
		}
	}

	timer_start(breath->delay_ms);
}

static void slide_init(void)
{
	memset(&yam.slide, 0, sizeof(struct yam_stat_slide));
	slide_update();
}

static void slide_update(void)
{
	const struct yam_cfg_slide *slide = &yam.req->slide;

	taskENTER_CRITICAL();
	yam_write(slide->pixels + yam.slide.step, YAM_PIXEL_NUM - yam.slide.step);
	yam_write(slide->pixels, yam.slide.step);
	taskEXIT_CRITICAL();

	if (YAM_DIRECTION_INC == yam.req->slide.dir) {
		if ((YAM_PIXEL_NUM - 1) == yam.slide.step) {
			if (slide->link) {
				yam_command(slide->link);
				return;
			} else {
				yam.slide.step = 0;
			}
		} else {
			yam.slide.step++;
		}
	} else {
		if (0 == yam.slide.step) {
			if (slide->link) {
				yam_command(slide->link);
				return;
			} else {
				yam.slide.step = YAM_PIXEL_NUM - 1;
			}
		} else {
			yam.slide.step--;
		}
	}

	timer_start(slide->delay_ms);
}

static void pingpong_init(void)
{
	memset(&yam.pingpong, 0, sizeof(struct yam_stat_pingpong));
	if (YAM_DIRECTION_INC == yam.req->pingpong.initial_dir){
	} else {
		yam.pingpong.dir = YAM_DIRECTION_DEC;
		yam.pingpong.pos = YAM_PIXEL_NUM - 1;
	}
	pingpong_update();
}

static void pixels_dim(struct yam_pixel *pixel)
{
	for (int i = 0; i < YAM_PIXEL_NUM; i++) {
		pixel[i].r >>= 1;
		pixel[i].g >>= 1;
		pixel[i].b >>= 1;
	}
}

static void pingpong_update(void)
{
	const struct yam_cfg_pingpong *pp = &yam.req->pingpong;

	pixels_dim(yam.pingpong.pixels);
	yam.pingpong.pixels[yam.pingpong.pos] = pp->initial_value;
	yam_update(yam.pingpong.pixels);

	if (YAM_DIRECTION_INC == yam.pingpong.dir) {
		if (yam.pingpong.step < pp->steps) {
			yam.pingpong.step++;
		} else if (pp->link_top) {
			yam_command(pp->link_top);
			return;
		} else {
			yam.pingpong.dir = YAM_DIRECTION_DEC;
			yam.pingpong.step = 0;
		}
		if (yam.pingpong.pos < (YAM_PIXEL_NUM - 1)) {
			yam.pingpong.pos++;
		}
	} else {
		if (yam.pingpong.step < pp->steps) {
			yam.pingpong.step++;
		} else if (pp->link_bot) {
			yam_command(pp->link_bot);
			return;
		} else {
			yam.pingpong.dir = YAM_DIRECTION_INC;
			yam.pingpong.step = 0;
		}
		if (yam.pingpong.pos > 0) {
			yam.pingpong.pos--;
		}
	}

	timer_start(yam.req->pingpong.delay_ms);
}

static void strip_anim_tsk(void *arg)
{
	(void)arg;
	uint32_t event = 0;

	while (true) {
		xTaskNotifyWait(0, 0xFFFFFFFF, &event, portMAX_DELAY);
		if (event & YAM_EVENT_UPDATE) {
			yam.req = yam.pend;
			init_fn[yam.req->mode]();
		} else if (event & YAM_EVENT_TIMER) {
			update_fn[yam.req->mode]();
		}
	}
}

static void IRAM_ATTR hw_timer_isr(void *arg)
{
	(void)arg;
	BaseType_t context_sw = pdFALSE;

	xTaskNotifyFromISR(yam.tsk, YAM_EVENT_TIMER,
			eSetValueWithoutOverwrite, &context_sw);

	if (pdTRUE == context_sw) {
		taskYIELD();
	}
}

int yam_init(uint_fast8_t gpio)
{
	memset(&yam, 0, sizeof(yam));
	yam.gpio_mask = 1UL<<gpio;
	const gpio_config_t conf = {
		.mode = GPIO_MODE_OUTPUT,
		.pin_bit_mask = yam.gpio_mask
	};

	gpio_config(&conf);
	clr();

	hw_timer_init(hw_timer_isr, NULL);
	hw_timer_set_clkdiv(TIMER_CLKDIV_256);
	xTaskCreate(strip_anim_tsk, "strip_anim", 256, NULL,
			ANIM_TSK_PRIO, &yam.tsk);

	return !yam.tsk;
}

void yam_deinit(void)
{
	hw_timer_disarm();
	hw_timer_deinit();
	vTaskDelete(yam.tsk);
}

void yam_command(const struct yam_cmd_req *req)
{
	yam.pend = req;
	xTaskNotify(yam.tsk, YAM_EVENT_UPDATE, eSetValueWithOverwrite);
}

