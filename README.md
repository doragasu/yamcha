# Introduction

Yamcha: Yet Another Module for Colorful Happy Animations. This module allows to perform several simple animations using an ESP8266 chip and any WS2812 based LED strip. The module is prepared to be deployed as a component for the Espressif [ESP8266\_RTOS\_SDK](https://github.com/espressif/ESP8266_RTOS_SDK).

Currently the following simple animations are supported:

* Fixed: no animation at all, just display whatever colors you want and let them be.
* Breath: breathing animation, progressively fade in and out the pixels with the colors you choose.
* Slide: draw whatever color sequence you want and scroll in one direction.
* PingPong: choose a single pixel color and smoothly move the pixel in one direction, then in the opposite and repeat. While moving, the pixel leaves a waning trail.

One cool thing of the module is that animations can be linked with each other to create more complex ones. For example you can create two configurations, one with `breath` effect for a single pixel, and other with the `pingpong` animation, and link them so the pixel is progressively faded in, and then it starts to scroll using the pingpong effect. The transition between animations is done automatically by the module if you link correctly the two configurations.

# Usage

Configure this repository as a component for ESP8266\_RTOS\_SDK. Then include `yamcha.h`. The following example shows how to configure and start the pingpong effect, connecting the LED strip to GPIO12, and using an 8-LED strip:

```C
#include <yamcha.h>

void pingpong_start(void)
{
	// Initialize, use GPIO 12 for strip output. You have to call this once
	yam_init(12);

	// Configure a pingpong effect: 10 steps per direction and 50 ms delay
	static const struct yam_cmd_req req = {
		.mode = YAM_CMD_PINGPONG,
		.pingpong = {
			.initial_value = {.r = 255},
			.steps = 10,
			.delay_ms = 50
		}
	};

	// Start the pingpong effect
	yam_command(&req);
}
```

An important thing to note here is that the lifetime of the configuration structure must be as long as the time the animation is displayed (and thus we need the `static` attribute in the definition unless we are using a global variable or the function does not return before animation ends).

# Issues and limitations.

The module uses a software routine to update pixels. The ESP8266 has no RMT hardware support (as does the ESP32), so the only known way to update the pixels via hardware is using hacking through the I2S module. Unfortunately this requires tying the output to `GPIO3`, that is also the UART RX pin. This is pretty inconvenient for most of the nodemcu and similar boards, that connect this pin to the output of a USB to UART converter using usually a small resistor (typically 470 ohm). So to avoid problems, the module updates the pixels using a software routine. This routine requires disabling interrupts to avoid the timing required by ws2812 LEDs to be disrupted, but as NMI interrupts cannot be disabled, some glitches can appear on the LEDs during heavy traffic load. I might give it a try using the I2S hardware in the future, but in my experience, the software implementation works quite well.

Another limitation of the module is that the number of supported LEDs is currently defined in the `YAM_PIXEL_NUM` constant of the `strip_anim.h` header file. I might make this configurable in a future update, but currently you will have to change the number to suit your setup and rebuild the project.

# License

This program is provided with NO WARRANTY, under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/2.0/). I am not responsible in any way if the code does not work, or causes any harm. Make sure you read the warning above in this regard.

